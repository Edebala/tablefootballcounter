/*
IR Breakbeam sensor demo!
*/
#include "ShiftRegister74HC595.h"
#define SDI1 7
#define SCLK1 6
#define LOAD1 5
#define SDI2 10
#define SCLK2 9
#define LOAD2 8
#define DIGITS 2
// Pin 13: Arduino has an LED connected on pin 13
// Pin 11: Teensy 2.0 has the LED on pin 11
// Pin 6: Teensy++ 2.0 has the LED on pin 6
// Pin 13: Teensy 3.0 has the LED on pin 13
#define SENSORPIN2 4
#define SENSORPIN1 3

int value,digit1,digit2,digit3,digit4; 
uint8_t  digits[] = {B01000000, //0
                      B11111001, //1 
                      B10100100, //2
                      B10110000, //3 
                      B10011001, //4
                      B10010010, //5
                      B10000010, //6 
                      B11111000, //7
                      B10000000, //8
                      B10010000 //9
                     };
                        

int sensorState = 0, lastState1=0, lastState2=0, counter1 = 0, counter2 = 0;
void setup() {
// initialize the sensor pin as an input:
pinMode(SENSORPIN1, INPUT);
digitalWrite(SENSORPIN1, HIGH); // turn on the pullup
pinMode(SENSORPIN2, INPUT);
digitalWrite(SENSORPIN2, HIGH); // turn on the pullup
Serial.begin(9600);
}
void loop(){
sensorState = digitalRead(SENSORPIN1);
  if (!sensorState && lastState1) {
    counter1 ++;
    if(counter1>10)counter1=0;
    showNumber1(counter1);
    delay(2000);
  }
  lastState1 = sensorState;
  sensorState = digitalRead(SENSORPIN2);
  if (!sensorState && lastState2) {
    counter2 ++;
    if(counter2>10)counter2=0;
    showNumber2(counter2);
    delay(2000);
  }
  lastState2 = sensorState;
  delay(50);
  
  showNumber1(counter1);
  showNumber2(counter2);
}

// create shift register object (number of shift registers, data pin, clock pin, latch pin)
ShiftRegister74HC595 sr1 (DIGITS, SDI1, SCLK1, LOAD1); 
ShiftRegister74HC595 sr2 (DIGITS, SDI2, SCLK2, LOAD2); 

void showNumber1(int num)
{
    digit2=num % 10 ;
    digit1=(num / 10) % 10 ;
    //Send them to 7 segment displays
    uint8_t numberToPrint[]= {digits[digit2],digits[digit1]};
    sr1.setAll(numberToPrint);  
}
void showNumber2(int num)
{
    digit2=num % 10 ;
    digit1=(num / 10) % 10 ;
    //Send them to 7 segment displays
    uint8_t numberToPrint[]= {digits[digit2],digits[digit1]};
    sr2.setAll(numberToPrint);  
}
